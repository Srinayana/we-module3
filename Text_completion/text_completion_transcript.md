﻿<a name="br1"></a> 

GenAI transcript for markov chains:

what is markov chains principle

The core principle of a Markov chain is the **Markov property**. This property states

that the probability of moving to the next state depends only on the current state, not

on any of the previous states. In simpler terms, it's like saying "the future only

depends on the present, not the past" in the context of the sequence of events.

Imagine flipping a coin. If it's a fair coin, the probability of getting heads on the next

flip is always 50%, regardless of whether you got heads or tails on the previous flip.

That's a simple example of the Markov property in action.

Here are some key points about Markov chains:

● They are used to model sequences of events where the probability of the next

event depends on the current state.

● They are widely used in various fields like predicting text, analyzing financial

markets, and modeling weather patterns.

● They are represented using transition matrices that show the probabilities of

moving between different states.

I hope this explanation clarifies the principle behind Markov chains!

how is this used in auto completion or text generating

Markov chains can be a foundation for autocompletion and text generation, but it's

important to understand both its strengths and limitations. Here's how it works:

**Training:**

1\. A large corpus of text (books, articles, code) is fed into the system.

2\. The system analyzes the text, looking at the sequence of words. It builds a

database of how often one word follows another.



<a name="br2"></a> 

3\. For example, it might learn that the word "the" is very often followed by the

word "quick" (as in "the quick brown fox").

**Prediction:**

1\. When the user starts typing, the system looks at the last word entered

(current state).

2\. Based on the database, it identifies a list of words that frequently follow that

particular word.

3\. It assigns probabilities to each suggested word based on how often it

appeared after the current word in the training data.

4\. In our example, if the user types "the", the system might suggest "quick" with

a high probability and other words like "cat" or "dog" with lower probabilities.

**Limitations:**

● **Short-term memory:** Markov chains typically only consider a limited number

of previous words (often just the last one). This can lead to repetitive or

nonsensical suggestions if the context requires a longer view.

● **Limited creativity:** Since Markov chains rely on probabilities based on

existing data, they struggle to generate truly creative or original text.

**Real-world applications:**

● While not the most advanced technique today, Markov chains can still be a

good starting point for simple autocomplete features, especially in situations

where complex grammar or context isn't crucial.

● They can be helpful for generating ideas or starting drafts, but may require

further editing for coherence and flow.

**Beyond Markov chains:**



<a name="br3"></a> 

● Modern text generation and autocompletion systems often use more

sophisticated techniques like neural networks, which can analyze larger

contexts and capture more complex relationships between words.

In summary, Markov chains offer a simple and efficient way to predict the next word

based on the current one. While they have limitations, they can be a building block

for text generation and have found applications in various autocomplete features.

Write a python function generate(filename: str, start\_words:

list[str], chain\_length: int, num\_generated: int) -> str which

takes a filename, a chain length, a list of start words which

has to be exactly as long as the chain\_length (why?), and

an integer num\_generated and returns a sentence

num\_generated words long which sounds similar to the text

contained in filename.

Here's the Python function you requested:

Python

import random

def generate(filename: str, start\_words: list[str], chain\_length: int,

num\_generated: int) -> str:

"""

Generates a sentence similar to the text in a file using Markov

chains.

Args:

filename: Path to the text file used for training.



<a name="br4"></a> 

start\_words: List of words to start the generated sentence (must

be same length as chain\_length).

chain\_length: Number of words considered to predict the next

word.

num\_generated: Number of words to generate in the output

sentence.

Returns:

A sentence generated using the Markov chain model.

"""

\# Check if start\_words length matches chain\_length

if len(start\_words) != chain\_length:

raise ValueError("Length of start\_words must equal chain\_length")

\# Open the file and read the text

with open(filename, 'r') as f:

text = f.read()

\# Clean and split the text into words

words = text.lower().replace('\n', ' ').split()

\# Create a dictionary to store word transitions

transitions = {}



<a name="br5"></a> 

for i in range(len(words) - chain\_length):

current\_words = tuple(words[i:i+chain\_length]) # Create a tuple of

chain\_length words

next\_word = words[i + chain\_length]

if current\_words not in transitions:

transitions[current\_words] = []

transitions[current\_words].append(next\_word)

\# Generate the sentence

generated\_sentence = start\_words

for \_ in range(num\_generated):

\# Get the next word based on the current sequence

current\_sequence = tuple(generated\_sentence[-chain\_length:])

if current\_sequence not in transitions:

\# If not found in transitions, randomly pick a word from the

entire vocabulary

next\_word = random.choice(words)

else:

next\_word = random.choice(transitions[current\_sequence])

generated\_sentence.append(next\_word)

\# Return the generated sentence as a string

return ' '.join(generated\_sentence)



<a name="br6"></a> 

Explanation:

1\. **Function Definition:** The function generate takes four arguments:

○

○

filename: Path to the text file used for training the model.

start\_words: A list of words to begin the generated sentence (needs

to be the same length as chain\_length).

○

○

chain\_length: Number of words considered to predict the next word in

the sequence.

num\_generated: Number of words to generate in the final sentence.

2\. **Input Validation:** It checks if the length of start\_words matches

chain\_length. This ensures we have a proper starting sequence for the

Markov chain prediction.

3\. **Read Text:** The function opens the file specified by filename and reads the

content into a string.

4\. **Clean and Split:** The text is converted to lowercase, newline characters are

replaced with spaces, and finally split into individual words.

5\. **Build Transition Dictionary:** An empty dictionary transitions is created to

store word sequences and their following words. The code iterates through

the list of words, considering a sequence of chain\_length words at a time.

For each sequence, it creates a tuple (to ensure unique key in the dictionary)

and retrieves the next word in the text. The next word is appended to the list

associated with the sequence in the transitions dictionary. Essentially, this

builds a lookup of how often a particular sequence of words is followed by

another word in the text.

6\. **Generate Sentence:** An empty list generated\_sentence is initialized with the

start\_words. The loop iterates num\_generated times to generate the

sentence word by word. Inside the loop:



<a name="br7"></a> 

○ A tuple current\_sequence is created containing the last chain\_length

words from the generated\_sentence.

○ The transitions dictionary is checked for this sequence.

■ If the sequence is not found, it means this combination of words

may not have appeared in the training text. In that case, a

random word is chosen from the entire vocabulary (words) for

variety.

■ If the sequence is found, a random word is chosen from the list

of words that follow that sequence in the transitions

dictionary. This ensures the generated sentence maintains a

similar word order based on the training text.

○ The chosen next\_word is appended to the generated\_sentence list

provide some test cases to check the above code

Here are some test cases to check the functionality of the generate function:

**Test Case 1: Basic Functionality**

● filename: "test.txt" (containing a few simple sentences)

● start\_words: ["This", "is", "a"]

● chain\_length: 3

● num\_generated: 5

This test verifies if the function can generate a sentence starting with the provided

words and considering a sequence of 3 words for prediction. The expected output

will be a sentence that continues in a similar style to the text in "test.txt".

**Test Case 2: Edge Case - Short Text File**

● filename: "tiny.txt" (containing a single sentence with less than chain\_length

words)



<a name="br8"></a> 

● start\_words: ["Hello", "world"]

● chain\_length: 2

● num\_generated: 3

This test checks how the function handles a situation where the training text is very

short (less words than the chain\_length). Here, the function might entirely rely on

random word selection after the starting words due to the limited training data.

**Test Case 3: Edge Case - Missing Sequence in Transitions**

● filename: "news.txt" (containing a news article)

● start\_words: ["The", "stock", "market"]

● chain\_length: 3

● num\_generated: 4

This test checks the scenario where the specific sequence of starting words might

not be present frequently in the training text. The function should handle this by

randomly choosing a word from the entire vocabulary for the next word after the

starting sequence.

**Test Case 4: Validation Check - Incorrect Start Word Length**

● filename: "poem.txt" (containing a poem)

● start\_words: ["Once", "upon", "a"] # Incorrect length (should be 3 for

chain\_length 3)

● chain\_length: 3

● num\_generated: 4

This test case intentionally provides a list of starting words with a length different

from the chain\_length. The function should raise a ValueError indicating the

mismatch.

**Test Case 5: Optional - Custom Start Words (if desired)**



<a name="br9"></a> 

If your implementation allows for more flexibility, you can add a test case with a

custom start word list that doesn't necessarily follow the training text but still

generates a grammatically correct sentence.

By running these test cases, you can ensure the core functionality of the generate

function works as expected and handles edge cases appropriately. You can expand

on these tests based on the specific features and functionalities you want to verify.

